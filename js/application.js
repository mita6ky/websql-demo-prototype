/**
 * Master application
 * Includes Classes Env, App
 */

window.App = window.App || {};
window.Env = window.Env || {};


(function(App, Env, API){

    var UI = {},
        User = {};

    App.init = function initializeApplication () {
        User.init();
        UI.init();
        this.changeStates();
        
    };

    App.state = null;

    App.states = {
        'default': function notLoggedInUser () {
            //bind UI events
            if(UI.currentForm == 'register') {
                
            } else if(UI.currentForm == 'login'){
                
               
            }
        },

        'viewDocuments': function viewUserDocuments () {
            //bind UI events
        }
    };

    App.getCurrentState = function getAppCurrentState () {

       return User.loggedIn ? 'viewDocuments' : 'default';
    };

    App.changeStates = function changeAppState() {
        var state = this.getCurrentState();
        
        if(this.state !== state) {
            this.state = state;
            UI.events.unbind[this.state]();
            UI.events.bind[state]();
            
            App.states[state]();
        }
        
    };

    App.callBacks = {
        'login': function loginCallBack (result) {
            if(result.length) {
            }
        },

        'register': function registerCallBack (result) {
            
            if(!result.length) {
                
                API.user.add(username, password);
                User.status.set(result.id, result.username);
                //for UI th chage states - remove forms and dispaly documents markup
            }

        }
    };
    //refactor this
    var tempData = {};
    
    /***
     * Check for registered user
     * @param {String} userName
     * @param {String} userPass
     * returns void
     */
    function checkUser (userName, userPassword) {
        //refactor this
        tempData.userName = userName;
        tempData.userPassword = userPassword;
        
        API.user.check(userName, userPassword, App.callBacks[UI.currentForm]);
    }

    App.register = function registerUser (params) {

        
        if (params.username && params.password && params.password === params.password2) {

            checkUser(params.username, params.password);
            
        }
    };

    App.login = function loginUser (userField, passField) {

        // After SUCCSESS set USER constructor
        // body...

        var username = 'Viktor' || userField.value;
        var password = 'passss' || passField.value;

        console.log(username, password);

    }
    
   

    /**
     * User Object
     */

    User = {

        settings: {
            userName: null,
            userId: null
        },

        init: function initializeUser () {
            this.settings = this.status.get();
            this.loggedIn = this.settings.userName && this.settings.userId ? true : false;
        },

        status: {

            /**
             * Set user name and id
             * @param [String] userName
             * @param [String] id
             * returns void
             */
            set: function (userName, userId) {
                localStorage.userName = userName;
                localStorage.userId = userId;

                User.loggedIn = userName && userId ? true : false;
            },

            /**
             * Set user name and id
             * returns [Object] - userName & userId
             */
            get: function () {

                return {
                    userName : localStorage.userName,
                    userId   : localStorage.userId,
                }
            }
        },

        loggedIn: false
    };

    UI.elements = {};

    UI.init = function initializeUI () {
        UI.elements.usernameField = $( '#username'),
        UI.elements.passwordField  = $('#password'),
        UI.elements.passwordField2 = $('#password2'),
        UI.elements.usernameLoginField  = $('#usernameLogin'),
        UI.elements.passwordLoginField  = $('#passwordLogin'),

        UI.elements.registerSection = $( "#register" ),
        UI.elements.registerSectionButton = $( "#register button" ),
        UI.elements.loginSection = $( "#login" ),
        UI.elements.loginSectionButton = $( "#login button" ),
        UI.elements.navigation = $('.navbar-nav li');

    };

    //changed on click on the tab
    UI.currentForm = 'register';
    
    UI.changeForms = function () {
        
        if(UI.currentForm == 'register') {
            UI.elements.registerSection.show();
            UI.elements.loginSection.hide();
        } else if(UI.currentForm == 'login') {
            UI.elements.registerSection.hide();
            UI.elements.loginSection.show();
        }
        
    };

    UI.events = {
        bind: {
            'default': function bindDefaultUIEvents () {

                //set UI.currentForm when forms change
                UI.elements.navigation.on('click', function () {
                    if($(this).hasClass("active")) {
                        return;
                    } else {
                        var showForm = $(this).attr('data-form');

                        $(this).addClass('active')
                            .siblings('li').removeClass('active');

                        $('#forms').find('#' + showForm).show()
                                .siblings('.col-xs-6').hide();
                    }
                });
                
                UI.elements.registerSectionButton.on("click", function(e) {

                    App.register({
                        username: UI.elements.usernameField[0].value, 
                        password: UI.elements.passwordField[0].value, 
                        password2: UI.elements.passwordField2[0].value
                    });
                });
            },
            'viewDocuments': function bindViewDocumentsEvents () {

            }
        },

        unbind: {
            'default': function bindDefaultUIEvents () {

            },
            'viewDocuments': function bindViewDocumentsEvents () {

            }
        }
    };

    
    /**
     * Document ready
     */
    $(function() {

        App.init();
    });

})(App, Env, API);